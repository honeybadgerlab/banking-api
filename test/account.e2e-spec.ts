import { HttpStatus, INestApplication } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { useContainer } from 'class-validator';
import * as request from 'supertest';
import { AccountService } from '../src/account/account.service';

import { AppModule } from '../src/app.module';
import { AuthService } from '../src/auth/auth.service';
import { AccountEntity } from '../src/entities/account.entity';
import { UserEntity } from '../src/entities/user.entity';
import { SeederService } from '../src/seeder/seeder.service';
import { UserService } from '../src/user/user.service';

interface TestUser {
  user: UserEntity;
  accessToken: string;
  accounts: Array<AccountEntity>;
}
describe('AccountController (e2e)', () => {
  let app: INestApplication;
  let seederService: SeederService;
  let userService: UserService;
  let accountService: AccountService;
  let authService: AuthService;

  const customerData: TestUser = {
    user: null,
    accessToken: '',
    accounts: [],
  };

  const customer1Data: TestUser = {
    user: null,
    accessToken: '',
    accounts: [],
  };

  const bankerData: TestUser = {
    user: null,
    accessToken: '',
    accounts: [],
  };

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot({ envFilePath: '.env.e2e', isGlobal: true }),
        AppModule,
      ],
    }).compile();

    seederService = moduleFixture.get<SeederService>(SeederService);
    userService = moduleFixture.get<UserService>(UserService);
    accountService = moduleFixture.get<AccountService>(AccountService);
    authService = moduleFixture.get<AuthService>(AuthService);

    app = moduleFixture.createNestApplication();
    useContainer(app.select(AppModule), { fallbackOnErrors: true });
    await app.init();
    await seederService.seedDatabase();

    customerData.user = await userService.findOneByUserName('customer');
    customerData.accessToken = await authService.getAccessToken(
      customerData.user,
    );

    customerData.accounts.push(
      await accountService.createAccount(
        {
          accountNumber: '000000000001',
          currency: 'USD',
          customerId: customerData.user.id,
          initialAmount: 100,
        },
        customerData.user.id,
      ),
    );

    customerData.accounts.push(
      await accountService.createAccount(
        {
          accountNumber: '000000000002',
          currency: 'USD',
          customerId: customerData.user.id,
          initialAmount: 10,
        },
        customerData.user.id,
      ),
    );

    customer1Data.user = await userService.findOneByUserName('customer1');
    customer1Data.accessToken = await authService.getAccessToken(
      customer1Data.user,
    );

    customer1Data.accounts.push(
      await accountService.createAccount(
        {
          accountNumber: '000000000003',
          currency: 'USD',
          customerId: customer1Data.user.id,
          initialAmount: 10,
        },
        customerData.user.id,
      ),
    );

    bankerData.user = await userService.findOneByUserName('banker');
    bankerData.accessToken = await authService.getAccessToken(bankerData.user);
  });

  afterAll(async () => {
    await seederService.resetDatabase();
    await app.close();
  });

  it('Customer can not access customer accounts endpoint', async () => {
    const data = await request(app.getHttpServer())
      .get(`/accounts/customer/${customerData.user.id}`)
      .set('Authorization', `Bearer ${customerData.accessToken}`)
      .send()
      .expect(HttpStatus.FORBIDDEN);
  });

  it('Bank employee can get customer accounts', async () => {
    const data = await request(app.getHttpServer())
      .get(`/accounts/customer/${customerData.user.id}`)
      .set('Authorization', `Bearer ${bankerData.accessToken}`)
      .send()
      .expect(HttpStatus.OK);
    expect(data.body.length >= 2);
  });

  it('Customer can not access account details by account number endpoint', async () => {
    const data = await request(app.getHttpServer())
      .get(`/accounts/customer/${customerData.accounts[0].accountNumber}`)
      .set('Authorization', `Bearer ${customerData.accessToken}`)
      .send()
      .expect(HttpStatus.FORBIDDEN);
  });

  it('Bank employee can get account detail by account number', async () => {
    const data = await request(app.getHttpServer())
      .get(`/accounts/customer/${customerData.user.id}`)
      .set('Authorization', `Bearer ${bankerData.accessToken}`)
      .send()
      .expect(HttpStatus.OK);
    const { body } = data;
    expect(body).toBeInstanceOf(Array);
    const accountInfo = body[0];
    expect(accountInfo.accountNumber).toBeDefined();
    expect(accountInfo.currency).toBeDefined();
    expect(accountInfo.balance).toEqual(expect.any(Number));
  });

  it('Customer can not get transaction history', async () => {
    await request(app.getHttpServer())
      .get(
        `/accounts/${customerData.accounts[0].accountNumber}/transaction-history`,
      )
      .set('Authorization', `Bearer ${customerData.accessToken}`)
      .send()
      .expect(HttpStatus.FORBIDDEN);
  });

  it('Bank employee can get transaction history', async () => {
    const data = await request(app.getHttpServer())
      .get(
        `/accounts/${customerData.accounts[0].accountNumber}/transaction-history`,
      )
      .set('Authorization', `Bearer ${bankerData.accessToken}`)
      .send()
      .expect(HttpStatus.OK);
    const { body } = data;
    expect(body.total > 0);
    expect(body.page).toEqual(expect.any(Number));
    expect(body.limit).toEqual(expect.any(Number));
    expect(body.data).toBeDefined();
    expect(body.data.length > 0);
  });

  it('Customer can not create account', async () => {
    await request(app.getHttpServer())
      .post(`/accounts`)
      .send({
        currency: 'USD',
        initialAmount: 10,
        customerId: 1,
        accountNumber: '012345678910',
      })
      .set('Authorization', `Bearer ${customerData.accessToken}`)
      .send()
      .expect(HttpStatus.FORBIDDEN);
  });

  it('Bank employee can create account with pre-defined account number', async () => {
    const data = await request(app.getHttpServer())
      .post(`/accounts/`)
      .set('Authorization', `Bearer ${bankerData.accessToken}`)
      .send({
        currency: 'USD',
        initialAmount: 10,
        customerId: customerData.user.id,
        accountNumber: '000000000004',
      })
      .expect(HttpStatus.CREATED);
    const { body } = data;
    expect(body.accountNumber).toBeDefined();
    expect(body.accountNumber === '000000000004');
    expect(body.currency).toBeDefined();
    expect(body.balance).toEqual(expect.any(Number));
    expect(body.owner).toBeDefined();
    expect(body.owner.id === customerData.user.id);
  });

  it('Bank employee can create account without account number, a random account number with length = 12 will be generated.', async () => {
    const data = await request(app.getHttpServer())
      .post(`/accounts`)
      .set('Authorization', `Bearer ${bankerData.accessToken}`)
      .send({
        currency: 'USD',
        initialAmount: 10,
        customerId: customerData.user.id,
      })
      .expect(HttpStatus.CREATED);
    const { body } = data;
    expect(body.accountNumber).toBeDefined();
    expect(body.accountNumber.length === 12);
    expect(body.currency).toBeDefined();
    expect(body.balance).toEqual(expect.any(Number));
    expect(body.owner).toBeDefined();
    expect(body.owner.id === customerData.user.id);
  });

  it('Bank employee can not create account with duplicate account number', async () => {
    await request(app.getHttpServer())
      .post(`/accounts`)
      .set('Authorization', `Bearer ${bankerData.accessToken}`)
      .send({
        currency: 'USD',
        initialAmount: 10,
        customerId: customerData.user.id,
        accountNumber: '000000000005',
      })
      .expect(HttpStatus.CREATED);
    await request(app.getHttpServer())
      .post(`/accounts`)
      .set('Authorization', `Bearer ${bankerData.accessToken}`)
      .send({
        currency: 'USD',
        initialAmount: 10,
        customerId: customerData.user.id,
        accountNumber: '000000000005',
      })
      .expect(HttpStatus.BAD_REQUEST);
  });

  it('Bank employee can not create account with initial amount less than 0', async () => {
    await request(app.getHttpServer())
      .post(`/accounts`)
      .set('Authorization', `Bearer ${bankerData.accessToken}`)
      .send({
        currency: 'USD',
        initialAmount: -10,
        customerId: customerData.user.id,
        accountNumber: '000000000006',
      })
      .expect(HttpStatus.BAD_REQUEST);
  });

  it('Bank employee can not create account with unsupported currency', async () => {
    await request(app.getHttpServer())
      .post(`/accounts`)
      .set('Authorization', `Bearer ${bankerData.accessToken}`)
      .send({
        currency: 'GPB',
        initialAmount: 10,
        customerId: customerData.user.id,
        accountNumber: '000000000006',
      })
      .expect(HttpStatus.BAD_REQUEST);
  });

  it('Bank employee can not create account with a customer that is not exist', async () => {
    await request(app.getHttpServer())
      .post(`/accounts`)
      .set('Authorization', `Bearer ${bankerData.accessToken}`)
      .send({
        currency: 'USD',
        initialAmount: 10,
        customerId: -1,
        accountNumber: '000000000006',
      })
      .expect(HttpStatus.BAD_REQUEST);
  });

  it('Customer can not deposit money', async () => {
    await request(app.getHttpServer())
      .post(`/accounts/${customerData.accounts[0].accountNumber}/deposit`)
      .set('Authorization', `Bearer ${customerData.accessToken}`)
      .send({
        currency: 'USD',
        amount: 10,
        note: 'Note',
      })
      .expect(HttpStatus.FORBIDDEN);
  });

  it('Bank employee can not deposit money with amount less than 0.1 USD', async () => {
    await request(app.getHttpServer())
      .post(`/accounts/${customerData.accounts[0].accountNumber}/deposit`)
      .set('Authorization', `Bearer ${bankerData.accessToken}`)
      .send({
        currency: 'USD',
        amount: 0,
        note: 'Note',
      })
      .expect(HttpStatus.BAD_REQUEST);
  });

  it('Bank employee can not deposit money with unsupported currency', async () => {
    await request(app.getHttpServer())
      .post(`/accounts/${customerData.accounts[0].accountNumber}/deposit`)
      .set('Authorization', `Bearer ${bankerData.accessToken}`)
      .send({
        currency: 'GPB',
        amount: 0,
        note: 'Note',
      })
      .expect(HttpStatus.BAD_REQUEST);
  });

  it('Bank employee can not deposit money with wrong account number', async () => {
    await request(app.getHttpServer())
      .post(`/accounts/999999999999/deposit`)
      .set('Authorization', `Bearer ${bankerData.accessToken}`)
      .send({
        currency: 'GPB',
        amount: 0,
        note: 'Note',
      })
      .expect(HttpStatus.BAD_REQUEST);
  });

  it('Bank employee can deposit in case all information is valid', async () => {
    const accountBefore = await accountService.getAccountByAccountNumber(
      customerData.accounts[0].accountNumber,
    );
    await request(app.getHttpServer())
      .post(`/accounts/${customerData.accounts[0].accountNumber}/deposit`)
      .set('Authorization', `Bearer ${bankerData.accessToken}`)
      .send({
        currency: 'USD',
        amount: 10,
        note: 'Note',
      })
      .expect(HttpStatus.CREATED);
    const accountAfter = await accountService.getAccountByAccountNumber(
      customerData.accounts[0].accountNumber,
    );

    expect(accountAfter.balance - accountBefore.balance === 10);
  });

  it('Customer can not withdraw money', async () => {
    await request(app.getHttpServer())
      .post(`/accounts/${customerData.accounts[0].accountNumber}/withdrawal`)
      .set('Authorization', `Bearer ${customerData.accessToken}`)
      .send({
        currency: 'USD',
        amount: 10,
      })
      .expect(HttpStatus.FORBIDDEN);
  });

  it('Bank employee can not withdraw money with amount less than 0.1 usd', async () => {
    await request(app.getHttpServer())
      .post(`/accounts/${customerData.accounts[0].accountNumber}/withdrawal`)
      .set('Authorization', `Bearer ${bankerData.accessToken}`)
      .send({
        currency: 'USD',
        amount: 0,
        note: 'Note',
      })
      .expect(HttpStatus.BAD_REQUEST);
  });

  it('Bank employee can not withdraw money with unsupported currency', async () => {
    await request(app.getHttpServer())
      .post(`/accounts/${customerData.accounts[0].accountNumber}/withdrawal`)
      .set('Authorization', `Bearer ${bankerData.accessToken}`)
      .send({
        currency: 'GPB',
        amount: 0,
        note: 'Note',
      })
      .expect(HttpStatus.BAD_REQUEST);
  });

  it('Bank employee can not withdraw money with wrong account number', async () => {
    await request(app.getHttpServer())
      .post(`/accounts/999999999999/withdrawal`)
      .set('Authorization', `Bearer ${bankerData.accessToken}`)
      .send({
        currency: 'GPB',
        amount: 0,
        note: 'Note',
      })
      .expect(HttpStatus.BAD_REQUEST);
  });

  it('Bank employee can withdraw money in case all information is valid', async () => {
    const accountBefore = await accountService.getAccountByAccountNumber(
      customerData.accounts[0].accountNumber,
    );
    await request(app.getHttpServer())
      .post(`/accounts/${customerData.accounts[0].accountNumber}/withdrawal`)
      .set('Authorization', `Bearer ${bankerData.accessToken}`)
      .send({
        currency: 'USD',
        amount: 10,
        note: 'Note',
      })
      .expect(HttpStatus.CREATED);
    const accountAfter = await accountService.getAccountByAccountNumber(
      customerData.accounts[0].accountNumber,
    );

    expect(accountAfter.balance - accountBefore.balance === 10);
  });

  it('Customer can not transfer money', async () => {
    await request(app.getHttpServer())
      .post(`/accounts/${customerData.accounts[0].accountNumber}/transfer`)
      .set('Authorization', `Bearer ${customerData.accessToken}`)
      .send({})
      .expect(HttpStatus.FORBIDDEN);
  });

  it('Bank employee can not transfer money with amount less than 0.1 usd', async () => {
    await request(app.getHttpServer())
      .post(`/accounts/${customerData.accounts[0].accountNumber}/transfer`)
      .set('Authorization', `Bearer ${bankerData.accessToken}`)
      .send({
        destinationAccountNumber: customerData.accounts[1].accountNumber,
        currency: 'USD',
        amount: 0,
        note: 'Note',
      })
      .expect(HttpStatus.BAD_REQUEST);
  });

  it('Bank employee can not transfer money with unsupported currency', async () => {
    await request(app.getHttpServer())
      .post(`/accounts/${customerData.accounts[0].accountNumber}/transfer`)
      .set('Authorization', `Bearer ${bankerData.accessToken}`)
      .send({
        destinationAccountNumber: customerData.accounts[1].accountNumber,
        currency: 'GPB',
        amount: 0,
        note: 'Note',
      })
      .expect(HttpStatus.BAD_REQUEST);
  });

  it('Bank employee can not transfer money with wrong destination account', async () => {
    await request(app.getHttpServer())
      .post(`/accounts/${customerData.accounts[0].accountNumber}/transfer`)
      .set('Authorization', `Bearer ${bankerData.accessToken}`)
      .send({
        destinationAccountNumber: '999999999',
        currency: 'USD',
        amount: 10,
        note: 'Note',
      })
      .expect(HttpStatus.BAD_REQUEST);
  });

  it('Bank employee can transfer money in case all information is valid', async () => {
    const sourceAccountBeforeTransfer =
      await accountService.getAccountByAccountNumber(
        customerData.accounts[0].accountNumber,
      );
    const destinationAccountBeforeTransfer =
      await accountService.getAccountByAccountNumber(
        customerData.accounts[1].accountNumber,
      );
    await request(app.getHttpServer())
      .post(`/accounts/${customerData.accounts[0].accountNumber}/transfer`)
      .set('Authorization', `Bearer ${bankerData.accessToken}`)
      .send({
        destinationAccountNumber: customerData.accounts[1].accountNumber,
        currency: 'USD',
        amount: 10,
        note: 'Note for transaction',
      })
      .expect(HttpStatus.CREATED);
    const sourceAccountAfterTransfer =
      await accountService.getAccountByAccountNumber(
        customerData.accounts[0].accountNumber,
      );
    const destinationAccountAfterTransfer =
      await accountService.getAccountByAccountNumber(
        customerData.accounts[1].accountNumber,
      );
    expect(
      sourceAccountAfterTransfer.balance -
        sourceAccountBeforeTransfer.balance ===
        10,
    );
    expect(
      destinationAccountAfterTransfer.balance -
        destinationAccountBeforeTransfer.balance ===
        10,
    );
  });
});
