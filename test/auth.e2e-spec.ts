import { HttpStatus, INestApplication } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { useContainer } from 'class-validator';
import * as request from 'supertest';

import { AppModule } from '../src/app.module';
import { SeederService } from '../src/seeder/seeder.service';

describe('AuthController (e2e)', () => {
  let app: INestApplication;
  let seederService: SeederService;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot({ envFilePath: '.env.e2e', isGlobal: true }),
        AppModule,
      ],
    }).compile();

    seederService = moduleFixture.get<SeederService>(SeederService);
    app = moduleFixture.createNestApplication();
    useContainer(app.select(AppModule), { fallbackOnErrors: true });
    await app.init();
    await seederService.seedDatabase();
  });

  afterAll(async () => {
    await seederService.resetDatabase();

    await app.close();
  });

  it('Customer can login', async () => {
    const data = await request(app.getHttpServer())
      .post('/auth/login')
      .send({ username: 'customer', password: '123456' })
      .expect(HttpStatus.CREATED);
    expect(data.body.accessToken.length > 0);
  });

  it('Banker can login', async () => {
    const data = await request(app.getHttpServer())
      .post('/auth/login')
      .send({ username: 'banker', password: '123456' })
      .expect(HttpStatus.CREATED);
    expect(data.body.accessToken.length > 0);
  });

  it('User can not login with wrong username or password', async () => {
    await request(app.getHttpServer())
      .post('/auth/login')
      .expect(HttpStatus.UNAUTHORIZED);
  });
});
