## Prerequisite
1. You should have PostgreSQL installed in your local machine.
2. Update `.env` and `.env.e2e` files to map with your PostgreSQL instance.
3. Create the database name `bank-db` for local development
4. Create the database name `test` for e2e test
## Installation

```bash
$ npm install
```
## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Swagger Document
- Go to [localhost:3000/swagger](http://localhost:3000/swagger) to get the api documents
## Description
Banking api provides basic features for the employees of the banks to perform these features:
- Login by username and password
- Create a new bank account for a customer, with an initial deposit amount. A single customer may have multiple bank accounts.
- Transfer amounts between any two accounts, including those owned by different customers.
- Deposit and withdraw money from a given account.
- Retrieve balances for a given account.
- Retrieve transfer history for a given account (with pagination and basic filters)
- Allow Bank employee to login into the system and use the endpoints

## Use cases
For now the banking api only allows customers to login only, we will extend it to be able to used by customers in the future.

![Use Cases](/documents/use-cases.png "Use Cases")

## Architecture
1. The backend api is written by NestJS
2. Database is PostgreSQL

![Architecture](/documents/architect.png "Architecture")

We will have a simple architect like this. For future development, we can introduce these following things:

- Auth Service. We are using a local strategy now, but for future we should switch to a separated Auth Service. It can be AWS Cognito, Keycloak, Auth0 or we can create our own Auth Service.
- Cache layer to speed up the application
- Queue to increase the application performance as well as decouple between layers.

## Database schema
![Database Schema](/documents/db-schema.png "Database Schema")

- Each user will have a username and password to be able to login into the system.
- Each user can have multiple accounts.
- Each account will have a unique account number with the length equals to 12.
- We will use the account number to get the information of the account or make transactions, since in normal day life. We often use account number to refer to our account instead of account id.
- Account will have the base currency, we allow only USD now. In the future we can have many kinds of currencies per account.
- Transactions will have sourceId and destinationId, as well as transaction type (Deposit, WithDraw, TransferMoney).
- For Deposit transaction, sourceId will be null and destinationId is required
- For Withdraw transaction, source id is required while destinationId is null.
- For TransferMoney transactions, sourceId and destinationId are required.

## How backend processes the requests?
![Process Request](/documents/process-request.png "Process Request")

The diagram above is the sample of how backend-api processes the request of a protected route.

1. Frontend makes the request to backend rest endpoints.
2. If this is the protected route, then JWTAuthGuard will be involved, if the access token is valid, then pass the request to the next middleware. Otherwise returns Forbidden result.
3. If the endpoint requires a specific role, for example, banker row. Then RolesGuard will be involved.
It will check if the user has the right role to perform the action. If yes, then pass the request to the next middleware. If not then return Forbidden result.
4. After that request will be put into the Data Validation Layer, to check if the request is valid. We have a built-in decorator to perform requests, or custom decorators based on our business's rules. For example IsSupportedCurrency, IsAccountNumberUnique, IsNotSameAsSourceAccount. If the request is valid then pass the request to the Service Layer. If not, return the validation errors to the frontend.
5. In the Service Layer, we will perform our business logic, and then return the result to frontend.

## Login

Users can login into the system using `POST /api/auth` endpoint.

For testing purpose we initial some users with password `123456`. Please check `seeder.service.ts`
```bash
# customer
$ username: 'ArishaBarron' || 'BrandenGibson' || 'RhondaChurch || 'customer' || 'customer1' 

banker
$ username: 'banker' 
```

Banker's login request
```shell
curl --location --request POST 'http://localhost:3000/api/auth/login' \
--header 'workspace-id: 1' \
--header 'Content-Type: application/json' \
--data-raw '{
    "password": "123456",
    "username": "banker"
}'
```
Response:
```json
{
  "accessToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwicm9sZSI6ImFnZW50IiwidXNlcm5hbWUiOiJzdHJpbmciLCJpYXQiOjE2MjM2ODYyNjIsImV4cCI6MTYyMzc3MjY2Mn0.4oiaRiWqPsGRAub1lVX7z98KNISOFNqwVrVCNbHmrIM
",
}
```

Customer's login request
```shell
curl --location --request POST 'http://localhost:3000/api/auth/login' \
--header 'workspace-id: 1' \
--header 'Content-Type: application/json' \
--data-raw '{
    "password": "123456",
    "username": "customer"
}'
```
Response:
```json
{
  "accessToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwicm9sZSI6ImFnZW50IiwidXNlcm5hbWUiOiJzdHJpbmciLCJpYXQiOjE2MjM2ODYyNjIsImV4cCI6MTYyMzc3MjY2Mn0.4oiaRiWqPsGRAub1lVX7z98KNISOFNqwVrVCNbHmrIM
",
}
```

## Create account for a given customer

```shell
curl --location --request POST 'http://localhost:3000/api/accounts/' \
--header 'workspace-id: 1' \
--header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6Niwicm9sZSI6ImJhbmtlciIsImlhdCI6MTY0ODczNDExNCwiZXhwIjoxNjQ4ODIwNTE0fQ.UG-8y5QanhzaujYkT0H9GJmPoAg6yrWL3qk2X_I0xFU' \
--header 'Content-Type: application/json' \
--data-raw '{
  "currency": "USD",
  "initialAmount": 10,
  "customerId": 1
}'

```
Response:
```json
{
    "accountNumber": "521781361173",
    "currency": "USD",
    "balance": 10,
    "owner": {
        "name": "Arisha Barron",
        "id": 1
    }
}
```

## Get accounts by customer
Get all accounts of customer with `id = 1`.
```shell
curl --location --request GET 'http://localhost:3000/api/accounts/customer/1' \
--header 'workspace-id: 1' \
--header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6Niwicm9sZSI6ImJhbmtlciIsImlhdCI6MTY0ODczNDExNCwiZXhwIjoxNjQ4ODIwNTE0fQ.UG-8y5QanhzaujYkT0H9GJmPoAg6yrWL3qk2X_I0xFU' \

```
Response:
```json
[
    {
        "id": 1,
        "accountNumber": "012345678911",
        "balance": 10,
        "currency": "USD"
    },
    {
        "id": 2,
        "accountNumber": "012345678910",
        "balance": 30,
        "currency": "USD"
    }
]
```

## Retrieve account information (balance) for a given account by account number
Get account information for account number = `012345678910`
```shell
curl --location --request GET 'http://localhost:3000/api/accounts/012345678910' \
--header 'workspace-id: 1' \
--header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6Niwicm9sZSI6ImJhbmtlciIsImlhdCI6MTY0ODczNDExNCwiZXhwIjoxNjQ4ODIwNTE0fQ.UG-8y5QanhzaujYkT0H9GJmPoAg6yrWL3qk2X_I0xFU' \


```
Response:
```json
{
    "accountNumber": "012345678910",
    "currency": "USD",
    "balance": 30,
    "owner": {
        "name": "Arisha Barron",
        "id": 1
    }
}
```

## Deposit money for a given account by account number
Deposit money for account number = `012345678910`
```shell
curl --location --request POST 'http://localhost:3000/api/accounts/012345678910/deposit' \
--header 'workspace-id: 1' \
--header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6Niwicm9sZSI6ImJhbmtlciIsImlhdCI6MTY0ODczNDExNCwiZXhwIjoxNjQ4ODIwNTE0fQ.UG-8y5QanhzaujYkT0H9GJmPoAg6yrWL3qk2X_I0xFU' \
--header 'Content-Type: application/json' \
--data-raw '{
  "currency": "USD",
  "amount": 10,
  "note": "note for transaction"
}'
```
Response:
```json
{
    "message": "Money is deposit successfully"
}
```

## Withdraw money for a given account by account number
Withdraw money for account number = `012345678910`
```shell
curl --location --request POST 'http://localhost:3000/api/accounts/012345678910/withdrawal' \
--header 'workspace-id: 1' \
--header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6Niwicm9sZSI6ImJhbmtlciIsImlhdCI6MTY0ODczNDExNCwiZXhwIjoxNjQ4ODIwNTE0fQ.UG-8y5QanhzaujYkT0H9GJmPoAg6yrWL3qk2X_I0xFU' \
--header 'Content-Type: application/json' \
--data-raw '{
  "currency": "USD",
  "amount": 10
}'
```
Response:
```json
{
    "message": "Money is withdrawn successfully"
}
```

## Transfer money from account to account
Transfer money from account number = `012345678910` to account number = `012345678911`
```shell
curl --location --request POST 'http://localhost:3000/api/accounts/012345678910/transfer' \
--header 'workspace-id: 1' \
--header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6Niwicm9sZSI6ImJhbmtlciIsImlhdCI6MTY0ODczNDExNCwiZXhwIjoxNjQ4ODIwNTE0fQ.UG-8y5QanhzaujYkT0H9GJmPoAg6yrWL3qk2X_I0xFU' \
--header 'Content-Type: application/json' \
--data-raw '{
  "destinationAccountNumber": "012345678911",
  "currency": "USD",
  "amount": 8,
  "note": "note for transaction"
}'
```
Response:
```json
{
    "message": "Money is transferred successfully"
}
```

## Retrieve transaction history for a given account (pagination and filter supported)
Get transaction history for account number = `012345678910`

For filter parameter, please check swagger document for more detail
```shell
curl --location --request GET 'http://localhost:3000/api/accounts/012345678910/transaction-history' \
--header 'workspace-id: 1' \
--header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6Niwicm9sZSI6ImJhbmtlciIsImlhdCI6MTY0ODczNDExNCwiZXhwIjoxNjQ4ODIwNTE0fQ.UG-8y5QanhzaujYkT0H9GJmPoAg6yrWL3qk2X_I0xFU' \
```
Response:
```json
{
    "total": 6,
    "page": 0,
    "limit": 10,
    "data": [
        {
            "sourceAccountNumber": "012345678910",
            "destinationAccountNumber": "012345678911",
            "transactionType": "TransferMoney",
            "currency": "USD",
            "amount": 8,
            "note": "note for transaction",
            "createdAt": "2022-03-31T21:24:07.452Z"
        },
        {
            "sourceAccountNumber": "012345678910",
            "destinationAccountNumber": "012345678911",
            "transactionType": "TransferMoney",
            "currency": "USD",
            "amount": 8,
            "note": "note for transaction",
            "createdAt": "2022-03-31T21:24:00.632Z"
        },
        {
            "sourceAccountNumber": "012345678910",
            "transactionType": "Withdraw",
            "currency": "USD",
            "amount": 10,
            "note": "",
            "createdAt": "2022-03-31T21:22:26.339Z"
        },
        {
            "destinationAccountNumber": "012345678910",
            "transactionType": "Deposit",
            "currency": "USD",
            "amount": 10,
            "note": "note for transaction",
            "createdAt": "2022-03-31T18:45:21.351Z"
        },
        {
            "destinationAccountNumber": "012345678910",
            "transactionType": "Deposit",
            "currency": "USD",
            "amount": 10,
            "note": "note for transaction",
            "createdAt": "2022-03-31T14:52:56.649Z"
        },
        {
            "destinationAccountNumber": "012345678910",
            "transactionType": "Deposit",
            "currency": "USD",
            "amount": 10,
            "note": "Initial deposit amount",
            "createdAt": "2022-03-31T14:52:51.936Z"
        }
    ]
}
```


