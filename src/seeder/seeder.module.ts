import { Module } from '@nestjs/common';
import { AccountModule } from '../account/account.module';
import { UserModule } from '../user/user.module';

import { OnSeederServiceInit } from './onSeederServiceInit';
import { SeederService } from './seeder.service';

@Module({
  controllers: [],
  imports: [
    UserModule,
    AccountModule
  ],
  providers: [SeederService, OnSeederServiceInit],
})
export class SeederModule {}
