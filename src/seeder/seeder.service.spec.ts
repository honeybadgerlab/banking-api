import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Connection, Repository } from 'typeorm';
import { UserEntity } from '../entities/user.entity';
import { SeederService } from './seeder.service';

describe('SeederService', () => {
  let seederService: SeederService;
  let userRepository: Repository<UserEntity>;
  let connection: Connection;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [],
      providers: [
        SeederService,
        {
          provide: getRepositoryToken(UserEntity),
          useValue: {
            findOne: jest.fn(),
            count: jest.fn(() => 0),
            save: jest.fn()
          },
        },
        {
          provide: Connection,
          useValue: {
            synchronize: jest.fn(),
            dropDatabase: jest.fn()
          },
        },
      ],
    }).compile();

    seederService = app.get<SeederService>(SeederService);
    userRepository = app.get<Repository<UserEntity>>(
      getRepositoryToken(UserEntity),
    );
    connection = app.get<Connection>(Connection);
  });

  it('should be defined', () => {
    expect(SeederService).toBeDefined();
  });

  it('userRepository should be defined', () => {
    expect(userRepository).toBeDefined();
  });

  describe('seedDatabase', () => {
    it('It should call userRepository.count and userRepositorySave', async () => {
      seederService.seedDatabase();
      expect(userRepository.count).toHaveBeenCalledTimes(1);
    });
  });

  describe('User service resetDatabase', () => {
    it('It should call dropDatabase and synchronize', async () => {
      seederService.resetDatabase();
      expect(connection.dropDatabase).toHaveBeenCalledTimes(1);
    });
  });
});
