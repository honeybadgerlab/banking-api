import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Connection, Repository } from 'typeorm';

import { UserEntity } from '../entities/user.entity';
import * as crypto from 'crypto';
import { RolesEnum } from '../common/constants/roles.enum';
@Injectable()
export class SeederService {
  constructor(
    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>,
    private connection: Connection,
  ) {}

  public async seedDatabase() {
    await this.seedUsers();
  }

  private async seedUsers() {
    const count = await this.userRepository.count();
    if (count > 0) {
      return;
    }

    console.log('SEEDING USERS');
    const users = [
      {
        name: 'Arisha Barron',
        username: 'ArishaBarron',
        role: RolesEnum.Customer,
      },
      {
        name: 'Branden Gibson',
        username: 'BrandenGibson',
        role: RolesEnum.Customer,
      },
      {
        name: 'Rhonda Church',
        username: 'RhondaChurch',
        role: RolesEnum.Customer,
      },
      {
        name: 'Georgina Hazel',
        username: 'Georgina Hazel',
        role: RolesEnum.Customer,
      },
      { name: 'Customer', username: 'customer', role: RolesEnum.Customer },
      { name: 'Customer 1', username: 'customer1', role: RolesEnum.Customer },
      { name: 'Banker', username: 'banker', role: RolesEnum.Banker },
    ];

    for (const user of users) {
      const salt = (Math.floor(Math.random() * 1000) + 1).toString();
      const password = crypto
        .createHmac('sha256', salt)
        .update('123456')
        .digest('hex');
      await this.userRepository.save({
        ...user,
        salt,
        password,
      });
    }
  }

  public async resetDatabase() {
    await this.connection.dropDatabase();
    await this.connection.synchronize();
  }
}
