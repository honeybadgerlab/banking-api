import { Injectable, OnModuleInit } from '@nestjs/common';
import { ModuleRef } from '@nestjs/core';

import { SeederService } from './seeder.service';

@Injectable()
export class OnSeederServiceInit implements OnModuleInit {
  private seederService: SeederService;

  constructor(private readonly moduleRef: ModuleRef) {}

  async onModuleInit() {
    this.seederService = await this.moduleRef.resolve(SeederService);
    await this.seederService.seedDatabase();
  }
}
