import { BadGatewayException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Connection, Repository } from 'typeorm';
import { AccountDetailsModel } from './models/account-details.model';
import { AccountModel } from './models/account.model';
import { CreateAccountModel } from './models/create-account.model';
import { DepositMoneyModel } from './models/deposit-money.model';
import { TransactionHistoryPaginationModel } from './models/transaction-history-pagination.model';
import { TransactionHistoryQueryModel } from './models/transaction-history-query.model';
import { TransactionHistoryModel } from './models/transaction-history.model';
import { TransferMoneyModel } from './models/transfer-money.model';
import { WithdrawMoneyModel } from './models/withdraw-money-model';
import { plainToClass } from 'class-transformer';
import { AccountEntity } from '../entities/account.entity';
import { TransactionEntity } from '../entities/transaction.entity';
import { TransactionTypesEnum } from '../common/constants/transaction-types.enum';
import { CurrentUserModel } from '../common/models/current-user.model';

@Injectable()
export class AccountService {
  constructor(
    @InjectRepository(AccountEntity)
    private accountRepository: Repository<AccountEntity>,
    @InjectRepository(TransactionEntity)
    private transactionRepository: Repository<TransactionEntity>,
    private connection: Connection,
  ) {}

  private async getQueryRunner() {
    const queryRunner = this.connection.createQueryRunner();
    await queryRunner.connect();
    return queryRunner;
  }
  async getUserAccounts(ownerId): Promise<AccountModel[]> {
    const accounts = await this.accountRepository.find({ where: { ownerId } });

    return accounts.map((t) => {
      return {
        id: t.id,
        accountNumber: t.accountNumber,
        balance: t.balance,
        currency: t.currency,
      };
    });
  }

  async getAccountByAccountNumber(accountNumber: string) {
    const account = await this.accountRepository.findOne({
      where: { accountNumber },
    });
    return account;
  }

  async getAccountDetail(accountNumber: string): Promise<AccountDetailsModel> {
    const account = await this.accountRepository.findOne({
      where: { accountNumber },
      join: {
        alias: 'account',
        innerJoinAndSelect: {
          owner: 'account.owner',
        },
      },
    });

    if (!account) {
      return null;
    }

    return {
      accountNumber: account.accountNumber,
      currency: account.currency,
      balance: account.balance,
      owner: { name: account.owner.name, id: account.ownerId },
    };
  }

  async getTransactionHistory(
    model: TransactionHistoryQueryModel,
  ): Promise<TransactionHistoryPaginationModel> {
    const currentAccount = await this.accountRepository.findOne({
      where: { accountNumber: model.accountNumber },
    });

    const queryBuilder = await this.transactionRepository
      .createQueryBuilder('transaction')
      .leftJoinAndSelect('transaction.source', 'source')
      .leftJoinAndSelect('transaction.destination', 'destination')
      .select([
        'transaction',
        'source.id',
        'source.accountNumber',
        'destination.id',
        'destination.accountNumber',
      ])
      .where(
        '(transaction.sourceId = :accountId OR transaction.destinationId = :accountId)',
        {
          accountId: currentAccount.id,
        },
      )
      .orderBy('transaction.createdAt', 'DESC');

    if (model.transactionType) {
      queryBuilder.andWhere('transaction.transactionType = :transactionType', {
        transactionType: model.transactionType,
      });
    }

    if (model.fromTime) {
      queryBuilder.andWhere('transaction.createdAt >= :fromTime', {
        fromTime: model.fromTime,
      });
    }

    if (model.toTime) {
      queryBuilder.andWhere('transaction.createdAt <= :toTime', {
        toTime: model.toTime,
      });
    }
    queryBuilder.skip(model.page * model.limit).take(model.limit);
    const [data, total] = await queryBuilder.getManyAndCount();

    const mappedData: TransactionHistoryModel[] = data.map((t) => {
      return {
        sourceAccountNumber: t.source?.accountNumber,
        destinationAccountNumber: t.destination?.accountNumber,
        transactionType: t.transactionType,
        currency: t.currency,
        amount: t.amount,
        note: t.note,
        createdAt: t.createdAt,
      };
    });
    return {
      total: total,
      page: model.page,
      limit: model.limit,
      data: mappedData,
    };
  }

  async isAccountNumberExist(accountNumber): Promise<boolean> {
    const existAccount = await this.accountRepository.findOne({
      where: { accountNumber },
    });
    if (existAccount) {
      return true;
    }
    return false;
  }

  /**
   * Create random and unique account number
   * This is just a demo function.
   * In production we should pre-generated the account number into a a table
   * Whenever we want to get random account number, we just need to lock the row.
   * And then take out the account number, after that we should delete the row that has been used.
   * @returns Account Number with length = 12
   */
  async createUniqueAccountNumber() {
    const generateRandomNumber = () => {
      const value =
        Math.floor(Math.random() * (999999999999 - 100000000000 + 1)) +
        100000000000;
      return value.toString();
    };

    let accountNumber = generateRandomNumber();

    while (await this.isAccountNumberExist(accountNumber)) {
      accountNumber = generateRandomNumber();
    }
    return accountNumber;
  }

  async createAccount(
    accountDetail: CreateAccountModel,
    createdUserId: number,
  ): Promise<AccountEntity> {
    if (!accountDetail.accountNumber) {
      accountDetail.accountNumber = await this.createUniqueAccountNumber();
    }

    const queryRunner = await this.getQueryRunner();
    await queryRunner.startTransaction();

    let newAccount: AccountEntity;
    try {
      const account = plainToClass(AccountEntity, {
        ...accountDetail,
        balance: accountDetail.initialAmount,
        ownerId: accountDetail.customerId,
        createdUserId,
      });

      const savedAccount = await queryRunner.manager.save(account);

      const transaction = plainToClass(TransactionEntity, {
        amount: accountDetail.initialAmount,
        currency: accountDetail.currency,
        destinationId: savedAccount.id,
        transactionType: TransactionTypesEnum.Deposit,
        note: 'Initial deposit amount',
      });

      await queryRunner.manager.save(transaction);
      await queryRunner.commitTransaction();

      newAccount = savedAccount;
    } catch (err) {
      await queryRunner.rollbackTransaction();
      throw err;
    } finally {
      await queryRunner.release();
    }
    return newAccount;
  }

  async deposit(model: DepositMoneyModel, user: CurrentUserModel) {
    await this.transferMoney(
      { ...model, transactionType: TransactionTypesEnum.Deposit },
      user,
    );
  }

  async withdraw(model: WithdrawMoneyModel, user: CurrentUserModel) {
    await this.transferMoney(
      { ...model, transactionType: TransactionTypesEnum.Withdraw },
      user,
    );
  }

  async transferMoney(model: TransferMoneyModel, user: CurrentUserModel) {
    const queryRunner = await this.getQueryRunner();
    await queryRunner.startTransaction('REPEATABLE READ');

    try {
      let sourceAccount;

      if (model.sourceAccountNumber) {
        sourceAccount = await queryRunner.manager.findOne(AccountEntity, {
          where: { accountNumber: model.sourceAccountNumber },
        });
        if (sourceAccount.balance < model.amount) {
          throw new BadGatewayException(
            'Current account does not have enough money to perform the operation',
          );
        }
        sourceAccount.balance -= model.amount;
        await queryRunner.manager.save(sourceAccount);
      }

      let destinationAccount;

      if (model.destinationAccountNumber) {
        destinationAccount = await queryRunner.manager.findOne(AccountEntity, {
          where: { accountNumber: model.destinationAccountNumber },
        });
        destinationAccount.balance += model.amount;
        await queryRunner.manager.save(destinationAccount);
      }
      await queryRunner.manager.save(
        plainToClass(TransactionEntity, {
          amount: model.amount,
          currency: model.currency,
          sourceId: sourceAccount?.id,
          destinationId: destinationAccount?.id,
          note: model.note,
          createdUserId: user.id,
          transactionType:
            model.transactionType || TransactionTypesEnum.TransferMoney,
        }),
      );
      await queryRunner.commitTransaction();
    } catch (err) {
      await queryRunner.rollbackTransaction();
      throw err;
    } finally {
      await queryRunner.release();
    }
  }
}
