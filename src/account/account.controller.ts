import {
  Body,
  Controller,
  Get,
  HttpStatus,
  Param,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import { ApiBadRequestResponse, ApiBearerAuth, ApiForbiddenResponse, ApiResponse, ApiTags } from '@nestjs/swagger';
import { RolesEnum } from '../common/constants/roles.enum';
import { Roles } from '../common/decorators/roles.decorator';
import { User } from '../common/decorators/user.decorator';
import { JwtAuthGuard } from '../common/guards/jwt-auth.guard';
import { RolesGuard } from '../common/guards/roles.guard';
import { ValidationErrorModel } from '../common/models/validationModel/validation-error.model';

import { AccountService } from './account.service';
import { AccountDetailsModel } from './models/account-details.model';
import { AccountModel } from './models/account.model';
import { CreateAccountModel } from './models/create-account.model';
import { DepositMoneyModel } from './models/deposit-money.model';
import { TransactionHistoryPaginationModel } from './models/transaction-history-pagination.model';
import { TransactionHistoryQueryModel } from './models/transaction-history-query.model';
import { TransferMoneyModel } from './models/transfer-money.model';
import { WithdrawMoneyModel } from './models/withdraw-money-model';
import { CreateAccountValidationPipe } from './validation-pipes/create-account-validation.pipe';
import { DepositMoneyValidationPipe } from './validation-pipes/deposit-money-validation.pipe';
import { TransactionHistoryValidationPipe } from './validation-pipes/transaction-history-validation.pipe';
import { TransferMoneyValidationPipe } from './validation-pipes/transfer-money-validation.pipe';
import { WithdrawMoneyValidationPipe } from './validation-pipes/withdraw-money-validation.pipe';

@ApiTags('accounts')
@Controller('accounts')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard, RolesGuard)
export class AccountController {
  constructor(
    private accountService: AccountService
  ) { }

  /**
   * Get all account for a given customer
   * @param user 
   * @returns 
   */
  @Get('customer/:customerId')
  @ApiResponse({
    description: 'Get all accounts for current logged in users',
    status: 200,
    type: [AccountModel],
  })
  @ApiForbiddenResponse({
    description: 'Only user with banker role can access this endpoint',
    status: HttpStatus.FORBIDDEN,
  })
  @Roles(
    RolesEnum.Banker
  )
  async getUserAccounts(
    @Param() param,
  ) {
    return await this.accountService.getUserAccounts(param.customerId);
  }

  /**
   * Get account details for a given account
   * 
   * @param param { accountNumber: string}
   * @returns 
   */
  @Get(':accountNumber')
  @ApiResponse({
    description: 'Get account detail by account number',
    status: 200,
    type: AccountDetailsModel,
  })
  @ApiForbiddenResponse({
    description: 'Only user with banker role can access this endpoint',
    status: HttpStatus.FORBIDDEN,
  })
  @ApiBadRequestResponse({ description: 'Validation error', type: ValidationErrorModel })
  @Roles(
    RolesEnum.Banker
  )
  async getAccountDetail(
    @Param() param
  ) {
    return await this.accountService.getAccountDetail(param.accountNumber);
  }

  @Get(':accountNumber/transaction-history')
  @ApiResponse({
    description: 'Account is created!',
    status: 200,
    type: TransactionHistoryPaginationModel
  })
  @ApiBadRequestResponse({ description: 'Validation error', type: ValidationErrorModel })
  @ApiForbiddenResponse({
    description: 'Only user with banker role can access this endpoint',
    status: HttpStatus.FORBIDDEN,
  })
  @ApiForbiddenResponse({
    description: 'There is a problem while retrieving transactions',
    status: HttpStatus.INTERNAL_SERVER_ERROR,
  })
  @Roles(
    RolesEnum.Banker
  )
  async getTransactionHistory(
    @Query(TransactionHistoryValidationPipe) model: TransactionHistoryQueryModel
  ) {
    return await this.accountService.getTransactionHistory(model);
  }

  @Post('')
  @ApiResponse({
    description: 'Account is created!',
    status: 200,
  })
  @ApiBadRequestResponse({ description: 'Validation error', type: ValidationErrorModel })
  @ApiForbiddenResponse({
    description: 'Only user with banker role can access this endpoint',
    status: HttpStatus.FORBIDDEN,
  })
  @ApiForbiddenResponse({
    description: 'There is a problem while creating account',
    status: HttpStatus.INTERNAL_SERVER_ERROR,
  })
  @Roles(
    RolesEnum.Banker
  )
  async createAccount(
    @Body(CreateAccountValidationPipe) body: CreateAccountModel,
    @User() user
  ) {
    const newAccount = await this.accountService.createAccount(body, user.id);
    return await this.accountService.getAccountDetail(newAccount.accountNumber);
  }

  @Post(':accountNumber/deposit')
  @ApiResponse({
    description: 'Money is deposited into account',
    status: 201,
  })
  @ApiBadRequestResponse({ description: 'Validation error', type: ValidationErrorModel })
  @ApiForbiddenResponse({
    description: 'There is a problem while depositing money',
    status: HttpStatus.INTERNAL_SERVER_ERROR,
  })
  @Roles(
    RolesEnum.Banker
  )
  async deposit(
    @Body(DepositMoneyValidationPipe) body: DepositMoneyModel,
    @User() user
  ) {
    await this.accountService.deposit(body, user);
    return { message: 'Money is deposit successfully' };
  }

  @Post(':accountNumber/withdrawal')
  @ApiResponse({
    description: 'Money is withdrawn',
    status: 201,
  })
  @ApiBadRequestResponse({ description: 'Validation error', type: ValidationErrorModel })
  @ApiForbiddenResponse({
    description: 'There is a problem while withdrawing money',
    status: HttpStatus.INTERNAL_SERVER_ERROR,
  })
  @Roles(
    RolesEnum.Banker
  )
  async withdraw(
    @Body(WithdrawMoneyValidationPipe) body: WithdrawMoneyModel,
    @User() user
  ) {
    await this.accountService.withdraw(body, user);
    return { message: 'Money is withdrawn successfully' };
  }

  @Post(':accountNumber/transfer')
  @ApiResponse({
    description: 'Money is transferred',
    status: 201,
  })
  @ApiBadRequestResponse({ description: 'Validation error', type: ValidationErrorModel })
  @ApiForbiddenResponse({
    description: 'There is a problem while transferring money',
    status: HttpStatus.INTERNAL_SERVER_ERROR,
  })
  @Roles(
    RolesEnum.Banker
  )
  async transferMoney(
    @Body(TransferMoneyValidationPipe) body: TransferMoneyModel,
    @User() user
  ) {
    await this.accountService.transferMoney(body, user);
    return { message: 'Money is transferred successfully' };
  }

}
