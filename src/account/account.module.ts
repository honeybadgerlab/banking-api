import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm/dist';
import { AccountEntity } from '../entities/account.entity';
import { TransactionEntity } from '../entities/transaction.entity';

import { UserModule } from '../user/user.module';
import { AccountController } from './account.controller';
import { AccountService } from './account.service';
import { IsAccountNumberExistConstraint } from './constraints/is-account-number-exist.constraint';
import { IsAccountNumberUniqueConstraint } from './constraints/is-account-number-unique.constraint';
import { IsNotSameAsSourceAccountConstraint } from './constraints/is-not-same-as-source-account.constraint';
import { IsSupportedCurrencyConstraint } from './constraints/is-supported-currency.constraint';

@Module({
  controllers: [AccountController],
  exports: [AccountService, TypeOrmModule],
  imports: [TypeOrmModule.forFeature([AccountEntity, TransactionEntity]), UserModule],
  providers: [AccountService, IsAccountNumberUniqueConstraint, IsSupportedCurrencyConstraint, IsAccountNumberExistConstraint, IsNotSameAsSourceAccountConstraint],
})
export class AccountModule { }