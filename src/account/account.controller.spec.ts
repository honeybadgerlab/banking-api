import { REQUEST } from '@nestjs/core';
import { Test, TestingModule } from '@nestjs/testing';
import { UserService } from '../user/user.service';
import { AccountController } from './account.controller';
import { AccountService } from './account.service';

describe('AccountController', () => {
  let accountController: AccountController;
  let accountService: AccountService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [],
      controllers: [AccountController],
      providers: [
        {
          provide: AccountService,
          useValue: {
            getUserAccounts: jest.fn(),
            getAccountDetail: jest.fn(),
            getTransactionHistory: jest.fn(() => []),
            createAccount: jest.fn(() => {
              return { accountNumber: 'mock' };
            }),
            deposit: jest.fn(),
            withdraw: jest.fn(),
            transferMoney: jest.fn(),
          },
        },
        {
          provide: REQUEST,
          useValue: {
            params: jest.fn(),
          },
        },
      ],
    }).compile();

    accountController = module.get<AccountController>(AccountController);
    accountService = module.get<AccountService>(AccountService);
  });

  it('should be defined', () => {
    expect(accountController).toBeDefined();
  });

  describe('Get all accounts of customers', () => {
    it('It should call accountService.getUserAccounts', async () => {
      accountController.getUserAccounts({ customerId: 1 });
      expect(accountService.getUserAccounts).toHaveBeenCalledTimes(1);
    });
  });

  describe('Get account detail by account number', () => {
    it('It should call accountService.getAccountDetail', async () => {
      accountController.getAccountDetail({ accountNumber: '01234567890' });
      expect(accountService.getAccountDetail).toHaveBeenCalledTimes(1);
    });
  });

  describe('Get transaction history by account number', () => {
    it('It should call accountService.getTransactionHistory', async () => {
      accountController.getTransactionHistory({ accountNumber: '01234567890' });
      expect(accountService.getTransactionHistory).toHaveBeenCalledTimes(1);
    });
  });

  describe('Create account', () => {
    it('It should call accountService.getTransactionHistory', async () => {
      accountController.createAccount(
        {
          accountNumber: '01234567890',
          currency: 'USD',
          customerId: 1,
          initialAmount: 10,
        },
        { id: 1 },
      );
      expect(accountService.createAccount).toHaveBeenCalledTimes(1);
    });
  });

  describe('deposit money by account number', () => {
    it('It should call accountService.deposit', async () => {
      accountController.deposit(
        {
          destinationAccountNumber: '01234567890',
          amount: 10,
          currency: 'USD',
          note: '',
        },
        { id: 1 },
      );
      expect(accountService.deposit).toHaveBeenCalledTimes(1);
    });
  });

  describe('withdraw money by account number', () => {
    it('It should call accountService.deposit', async () => {
      accountController.withdraw(
        {
          sourceAccountNumber: '01234567890',
          amount: 10,
          currency: 'USD',
        },
        { id: 1 },
      );
      expect(accountService.withdraw).toHaveBeenCalledTimes(1);
    });
  });

  describe('transfer money from account to another account', () => {
    it('It should call accountService.transferMoney', async () => {
      accountController.transferMoney(
        {
          sourceAccountNumber: '01234567890',
          destinationAccountNumber: '01234567891',
          amount: 10,
          currency: 'USD',
        },
        { id: 1 },
      );
      expect(accountService.transferMoney).toHaveBeenCalledTimes(1);
    });
  });
});
