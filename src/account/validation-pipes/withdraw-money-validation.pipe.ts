import { ArgumentMetadata, Inject, Injectable } from '@nestjs/common';
import { REQUEST } from '@nestjs/core';
import { plainToClass } from 'class-transformer';
import { BaseValidationPipe } from '../../common/validation-pipes/base-validation.pipe';
import { WithdrawMoneyModel } from '../models/withdraw-money-model';

@Injectable()
export class WithdrawMoneyValidationPipe extends BaseValidationPipe {
  constructor(
    @Inject(REQUEST) private readonly request: any
  ) {
    super();
  }

  async transform(value: any, metadata: ArgumentMetadata): Promise<any> {
    const entity = plainToClass(WithdrawMoneyModel, {
      ...value,
      sourceAccountNumber: this.request.params.accountNumber
    });
    return super.transform(entity, metadata);
  }
}
