import { ArgumentMetadata, BadRequestException, Inject, Injectable } from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { BaseValidationPipe } from '../../common/validation-pipes/base-validation.pipe';
import { CreateAccountModel } from '../models/create-account.model';

@Injectable()
export class CreateAccountValidationPipe extends BaseValidationPipe {
  constructor(
  ) {
    super();
  }

  async transform(value: any, metadata: ArgumentMetadata): Promise<any> {
    const entity = plainToClass(CreateAccountModel, {
      ...value
    });
    return super.transform(entity, metadata);
  }
}
