import { ArgumentMetadata, BadRequestException, Inject, Injectable } from '@nestjs/common';
import { REQUEST } from '@nestjs/core';
import { plainToClass } from 'class-transformer';
import { BaseValidationPipe } from '../../common/validation-pipes/base-validation.pipe';
import { TransactionHistoryQueryModel } from '../models/transaction-history-query.model';

@Injectable()
export class TransactionHistoryValidationPipe extends BaseValidationPipe {
  constructor(
    @Inject(REQUEST) private readonly request: any
  ) {
    super();
  }

  async transform(value: any, metadata: ArgumentMetadata): Promise<any> {
    const entity = plainToClass(TransactionHistoryQueryModel, {
      ...this.request.query,
      accountNumber: this.request.params.accountNumber,
      page: Number.parseInt(this.request.query.page || '0'),
      limit: Number.parseInt(this.request.query.limit || '10'),
    });
    return super.transform(entity, metadata);
  }
}
