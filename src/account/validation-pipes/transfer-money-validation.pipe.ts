import { ArgumentMetadata, Inject, Injectable } from '@nestjs/common';
import { REQUEST } from '@nestjs/core';
import { plainToClass } from 'class-transformer';
import { TransactionTypesEnum } from '../../common/constants/transaction-types.enum';
import { BaseValidationPipe } from '../../common/validation-pipes/base-validation.pipe';
import { TransferMoneyModel } from '../models/transfer-money.model';

@Injectable()
export class TransferMoneyValidationPipe extends BaseValidationPipe {
  constructor(
    @Inject(REQUEST) private readonly request: any
  ) {
    super();
  }

  async transform(value: any, metadata: ArgumentMetadata): Promise<any> {
    const entity = plainToClass(TransferMoneyModel, {
      ...value,
      sourceAccountNumber: this.request.params.accountNumber,
      transactionType: TransactionTypesEnum.TransferMoney
    });
    return super.transform(entity, metadata);
  }
}
