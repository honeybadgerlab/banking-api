import { ArgumentMetadata, Inject, Injectable } from '@nestjs/common';
import { REQUEST } from '@nestjs/core';
import { plainToClass } from 'class-transformer';
import { BaseValidationPipe } from '../../common/validation-pipes/base-validation.pipe';
import { DepositMoneyModel } from '../models/deposit-money.model';

@Injectable()
export class DepositMoneyValidationPipe extends BaseValidationPipe {
  constructor(
    @Inject(REQUEST) private readonly request: any
  ) {
    super();
  }

  async transform(value: any, metadata: ArgumentMetadata): Promise<any> {
    const entity = plainToClass(DepositMoneyModel, {
      ...value,
      destinationAccountNumber: this.request.params.accountNumber
    });
    return super.transform(entity, metadata);
  }
}
