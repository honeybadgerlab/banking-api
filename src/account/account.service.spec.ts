import { Test, TestingModule } from '@nestjs/testing';
import { AccountService } from './account.service';
import { Connection, QueryRunner, Repository } from 'typeorm';
import { AccountEntity } from '../entities/account.entity';
import { TransactionEntity } from '../entities/transaction.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { TransactionTypesEnum } from '../common/constants/transaction-types.enum';

class ConnectionMock {
  createQueryRunner(mode?: 'master' | 'slave'): QueryRunner {
    const qr = {
      manager: {},
    } as QueryRunner;
    qr.manager;
    Object.assign(qr.manager, {
      save: jest.fn(() => {
        return { id: 1 };
      }),
      findOne: jest.fn((entity, whereClause) => {
        if (
          whereClause.where?.accountNumber !== '012345678910' &&
          whereClause.where?.accountNumber !== '012345678911'
        ) {
          return null;
        }
        return {
          accountNumber: whereClause.where.accountNumber,
          balance: 10,
          currency: 'USD',
          ownerId: 1,
          owner: { name: 'test', id: 1 },
        };
      }),
    });
    qr.connect = jest.fn();
    qr.release = jest.fn();
    qr.startTransaction = jest.fn();
    qr.commitTransaction = jest.fn();
    qr.rollbackTransaction = jest.fn();
    qr.release = jest.fn();
    return qr;
  }
}

describe('AccountService', () => {
  let accountService: AccountService;
  let accountRepository: Repository<AccountEntity>;
  let transactionRepository: Repository<TransactionEntity>;
  let connection: Connection;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [],
      imports: [],
      providers: [
        AccountService,
        {
          provide: getRepositoryToken(AccountEntity),
          useValue: {
            find: jest.fn((id) => {
              if (id > 0) {
                const results = [];

                for (let i = 0; i < 3; i++) {
                  results.push({
                    id: i,
                    accountNumber: `0000000000${i}`,
                    balance: i,
                    currency: 'USD',
                  });
                }
                return results;
              }
              return [];
            }),
            findOne: jest.fn((whereClause) => {
              if (
                whereClause.where?.accountNumber !== '012345678910' &&
                whereClause.where?.accountNumber !== '012345678911'
              ) {
                return null;
              }
              return {
                accountNumber: whereClause.where.accountNumber,
                balance: 10,
                currency: 'USD',
                ownerId: 1,
                owner: { name: 'test', id: 1 },
              };
            }),
          },
        },
        {
          provide: getRepositoryToken(TransactionEntity),
          useValue: {
            createQueryBuilder: jest.fn(() => ({
              delete: jest.fn().mockReturnThis(),
              leftJoinAndSelect: jest.fn().mockReturnThis(),
              innerJoinAndSelect: jest.fn().mockReturnThis(),
              innerJoin: jest.fn().mockReturnThis(),
              from: jest.fn().mockReturnThis(),
              where: jest.fn().mockReturnThis(),
              execute: jest.fn().mockReturnThis(),
              getOne: jest.fn().mockReturnThis(),
              select: jest.fn().mockReturnThis(),
              andWhere: jest.fn().mockReturnThis(),
              orderBy: jest.fn().mockReturnThis(),
              skip: jest.fn().mockReturnThis(),
              take: () => ({
                skip: (cnt) => ({
                  skip: cnt,
                }),
              }),
              getManyAndCount: () => {
                const data = [];
                for (let i = 1; i < 10; i++) {
                  data.push({
                    source: {
                      accountNumber: `01234567891${i - 1}`,
                    },
                    destination: {
                      accountNumber: `01234567891${i + 1}`,
                    },
                    transactionType: 'TransferMoney',
                    currency: 'USD',
                    amount: i + Math.random() * 100,
                    note: `Transaction note ${i}`,
                    createdAt: new Date(),
                  });
                }
                return [data, 100];
              },
            })),
          },
        },
        {
          provide: Connection,
          useClass: ConnectionMock,
        },
      ],
    }).compile();

    accountService = app.get<AccountService>(AccountService);
    connection = app.get<Connection>(Connection);
    accountRepository = app.get<Repository<AccountEntity>>(
      getRepositoryToken(AccountEntity),
    );
    transactionRepository = app.get<Repository<TransactionEntity>>(
      getRepositoryToken(TransactionEntity),
    );
  });

  it('AccountService should be defined', () => {
    expect(accountService).toBeDefined();
  });

  it('accountRepository should be defined', () => {
    expect(accountRepository).toBeDefined();
  });

  it('transactionRepository should be defined', () => {
    expect(transactionRepository).toBeDefined();
  });

  it('connection should be defined', () => {
    expect(connection).toBeDefined();
  });

  describe(`Get user's account by user id`, () => {
    it('It get users account by user id', async () => {
      const accounts = await accountService.getUserAccounts(1);
      expect(accounts).toBeInstanceOf(Array);
      expect(accountRepository.find).toHaveBeenCalledTimes(1);
    });
  });

  describe(`Get account by account number`, () => {
    it('It get account by account number', async () => {
      const account = await accountService.getAccountByAccountNumber(
        '012345678910',
      );
      expect(account.accountNumber).toBeDefined();
      expect(account.balance).toBeDefined();
      expect(account.ownerId).toBeDefined();
      expect(account.currency).toBeDefined();
      expect(accountRepository.findOne).toHaveBeenCalledTimes(1);
    });
  });

  describe(`Get account detail`, () => {
    it('It get account by account number', async () => {
      const account = await accountService.getAccountDetail('012345678910');
      expect(account.accountNumber).toBeDefined();
      expect(account.balance).toBeDefined();
      expect(account.owner).toBeDefined();
      expect(account.currency).toBeDefined();
      expect(accountRepository.findOne).toHaveBeenCalledTimes(1);
    });
  });

  describe(`Get transaction history`, () => {
    it('It gets transactions history by account number', async () => {
      const data = await accountService.getTransactionHistory({
        accountNumber: '012345678910',
        fromTime: new Date(),
        toTime: new Date(),
        limit: 10,
        page: 0,
        transactionType: TransactionTypesEnum.TransferMoney,
      });

      expect(data.total).toEqual(100);
      expect(data.limit).toEqual(10);
      expect(data.page).toEqual(0);
      expect(data.data).toBeInstanceOf(Array);
      expect(accountRepository.findOne).toHaveBeenCalledTimes(1);
    });
  });

  describe(`Check if account number is exist`, () => {
    it('It should return true if account is exist', async () => {
      const data = await accountService.isAccountNumberExist('012345678910');
      expect(data).toEqual(true);
    });
    it('It should return false if account is not exist', async () => {
      const data = await accountService.isAccountNumberExist('012345678912');
      expect(data).toEqual(false);
    });
  });

  describe(`It should generate random unique account number`, () => {
    it('It should generate account number with 12 digits', async () => {
      const data = await accountService.createUniqueAccountNumber();
      expect(data.length).toEqual(12);
    });
  });

  describe(`It should create account`, () => {
    it('It should create new account with transaction', async () => {
      const data = await accountService.createAccount(
        {
          currency: 'USD',
          customerId: 1,
          initialAmount: 10,
          accountNumber: null,
        },
        1,
      );
      expect(data.id).toEqual(1);
    });
  });

  describe(`Deposit money`, () => {
    it('It should to deposit to account', async () => {
      await accountService.deposit(
        {
          amount: 10,
          currency: 'USD',
          destinationAccountNumber: '012345678910',
          note: '',
        },
        { id: 1 },
      );
    });
  });

  describe(`Withdraw money`, () => {
    it('It should allow with draw money from account', async () => {
      await accountService.withdraw(
        {
          amount: 10,
          currency: 'USD',
          sourceAccountNumber: '012345678910',
        },
        { id: 1 },
      );
    });
    it('It should not allow withdraw money from account if balance is not sufficient', async () => {
      try {
        await accountService.withdraw(
          {
            amount: 100,
            currency: 'USD',
            sourceAccountNumber: '012345678910',
          },
          { id: 1 },
        );
        // Fail test if above expression doesn't throw anything.
        expect(true).toBe(false);
      } catch (e) {
        expect(e.message).toBe(
          'Current account does not have enough money to perform the operation',
        );
      }
    });
  });

  describe(`Transfer money`, () => {
    it('It should allow transfer money from one account to another account', async () => {
      await accountService.transferMoney(
        {
          amount: 10,
          currency: 'USD',
          sourceAccountNumber: '012345678910',
          destinationAccountNumber: '012345678911',
        },
        { id: 1 },
      );
    });
    it('It should not allow transfer money from account if balance is not sufficient', async () => {
      try {
        await accountService.transferMoney(
          {
            amount: 100,
            currency: 'USD',
            sourceAccountNumber: '012345678910',
            destinationAccountNumber: '012345678911',
          },
          { id: 1 },
        );
        // Fail test if above expression doesn't throw anything.
        expect(true).toBe(false);
      } catch (e) {
        expect(e.message).toBe(
          'Current account does not have enough money to perform the operation',
        );
      }
    });
  });
});
