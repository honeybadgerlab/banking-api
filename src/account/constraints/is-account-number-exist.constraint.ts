import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { ModuleRef } from '@nestjs/core';
import {
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';

import { AccountService } from '../account.service';

@ValidatorConstraint({ async: true, name: 'IsAccountNumberExist' })
@Injectable()
export class IsAccountNumberExistConstraint implements ValidatorConstraintInterface {

  constructor(private accountService: AccountService) { }

  defaultMessage(validationArguments?: ValidationArguments): string {
    return `Account number ${validationArguments.value} is not exist`;
  }

  async validate(accountNumber: string, validationArguments?: ValidationArguments): Promise<boolean> {
    if (!accountNumber) {
      return true;
    }

    return await this.accountService.isAccountNumberExist(accountNumber);
  }
}

export function IsAccountNumberExist(validationOptions?: ValidationOptions) {
  return function (object: unknown, propertyName: string) {
    registerDecorator({
      constraints: [],
      options: validationOptions,
      propertyName,
      target: object.constructor,
      validator: IsAccountNumberExistConstraint,
    });
  };
}
