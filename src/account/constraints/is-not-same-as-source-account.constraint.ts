import { Injectable } from '@nestjs/common';
import {
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';


@ValidatorConstraint({ async: true, name: 'IsNotSameAsSourceAccount' })
@Injectable()
export class IsNotSameAsSourceAccountConstraint implements ValidatorConstraintInterface {

  constructor() { }

  defaultMessage(validationArguments?: ValidationArguments): string {
    return `Source Account Number and Destination Number Should Be different`;
  }

  async validate(destinationAccountNumber: string, validationArguments?: ValidationArguments): Promise<boolean> {
    return destinationAccountNumber !== validationArguments.object['sourceAccountNumber'];
  }
}

export function IsNotSameAsSourceAccount(validationOptions?: ValidationOptions) {
  return function (object: unknown, propertyName: string) {
    registerDecorator({
      constraints: [],
      options: validationOptions,
      propertyName,
      target: object.constructor,
      validator: IsNotSameAsSourceAccountConstraint,
    });
  };
}
