import { Injectable } from '@nestjs/common';
import {
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';


@ValidatorConstraint({ async: true, name: 'IsSupportedCurrency' })
@Injectable()
export class IsSupportedCurrencyConstraint implements ValidatorConstraintInterface {

  defaultMessage(validationArguments?: ValidationArguments): string {
    return `Only USD is supported at the moment`;
  }

  async validate(currency: string, validationArguments?: ValidationArguments): Promise<boolean> {
    return currency === 'USD';
  }
}

export function IsSupportedCurrency(validationOptions?: ValidationOptions) {
  return function (object: unknown, propertyName: string) {
    registerDecorator({
      constraints: [],
      options: validationOptions,
      propertyName,
      target: object.constructor,
      validator: IsSupportedCurrencyConstraint,
    });
  };
}
