import { ApiProperty } from '@nestjs/swagger';

export class AccountModel {

    @ApiProperty({
        description: 'accountNumber',
    })
    accountNumber: string;

    @ApiProperty({
        description: 'balance',
        example: '0.0'
    })
    balance?: number;

    @ApiProperty({
        description: 'currency',
        example: 'USD'
    })
    currency: string;
}
