import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsOptional } from "class-validator";
import { IsAccountNumberExist } from "../constraints/is-account-number-exist.constraint";

export class TransactionHistoryQueryModel {

    @IsNotEmpty()
    @IsAccountNumberExist()
    accountNumber: string

    @ApiProperty({
        description: 'Deposit, Withdraw, TransferMoney',
        example: 'TransferMoney',
        required: false,
    })
    transactionType?: string

    @ApiProperty({
        description: 'From Time',
        required: false,
        example: '2022-02-30T04:18:46.477Z'
    })
    @IsOptional()
    fromTime?: Date

    @ApiProperty({
        description: 'To Time',
        required: false,
        example: '2022-03-30T04:18:46.477Z'
    })
    @IsOptional()
    toTime?: Date

    @ApiProperty({
        description: 'limit',
        example: '10',
        required: false,
    })
    @IsOptional()
    limit?: number = 10;

    @ApiProperty({
        description: 'page',
        example: '0',
        required: false,
    })
    @IsOptional()
    page?: number = 0;
}
