import { ApiProperty } from "@nestjs/swagger";

export class TransactionHistoryModel {
    @ApiProperty({
        description: 'source account',
        example: '012345678910',
        required: false,
    })
    sourceAccountNumber: string;

    @ApiProperty({
        description: 'destination account number',
        example: '012345678910',
        required: false,
    })
    destinationAccountNumber: string;

    @ApiProperty({
        description: 'type of transaction for example Deposit, Withdraw, TransferMoney',
        example: 'TransferMoney',
        required: false,
    })
    transactionType: string;

    @ApiProperty({
        description: 'currency, for now only USD is supported',
        required: true,
        example: 'USD'
    })
    currency: string;

    @ApiProperty({
        description: 'amount of money, min value should be 10 cents',
        required: true,
        example: '10.0'
    })
    amount: number;

    @ApiProperty({
        description: 'note for transaction',
        required: true,
        example: 'note for transaction'
    })
    note: string;

    @ApiProperty({
        description: 'createdAt',
        required: true,
    })
    createdAt: Date;
}
