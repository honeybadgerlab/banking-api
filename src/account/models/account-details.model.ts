import { ApiProperty } from '@nestjs/swagger';
import { UserModel } from '../../user/models/user.model';
import { AccountModel } from './account.model';

export class AccountDetailsModel extends AccountModel {
    @ApiProperty({
        description: 'owner',
    })
    owner: UserModel;
}
