import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional, Length, Max, Min } from 'class-validator';
import { TransactionTypesEnum } from '../../common/constants/transaction-types.enum';
import { IsAccountNumberExist } from '../constraints/is-account-number-exist.constraint';
import { IsNotSameAsSourceAccount } from '../constraints/is-not-same-as-source-account.constraint';
import { IsSupportedCurrency } from '../constraints/is-supported-currency.constraint';

export class TransferMoneyModel {
  @IsNotEmpty()
  @IsAccountNumberExist()
  sourceAccountNumber?: string;

  @ApiProperty({
    description: 'destination account number',
    example: '012345678910',
    required: false,
  })
  @IsNotEmpty()
  @IsAccountNumberExist()
  @IsNotSameAsSourceAccount()
  destinationAccountNumber?: string;

  @ApiProperty({
    description: 'currency, for now only USD is supported',
    required: true,
    example: 'USD',
  })
  @IsNotEmpty()
  @IsSupportedCurrency()
  currency: string;

  @ApiProperty({
    description: 'amount of money, min value should be 10 cents',
    required: true,
    example: '10.0',
  })
  @IsNotEmpty()
  @Min(0.1)
  @Max(1000000000)
  amount: number;

  @ApiProperty({
    description: 'note for transaction',
    required: true,
    example: 'note for transaction',
  })
  @IsOptional()
  @Length(0, 256)
  note?: string;

  transactionType?: TransactionTypesEnum;
}
