import { ApiProperty } from "@nestjs/swagger";
import { TransactionHistoryModel } from "./transaction-history.model";

export class TransactionHistoryPaginationModel {
    @ApiProperty({
        description: 'limit',
        example: '10',
        required: false,
    })
    limit: number = 10;

    @ApiProperty({
        description: 'page',
        example: '0',
        required: false,
    })
    page: number = 0;

    @ApiProperty({
        description: 'total',
        example: '0',
        required: false,
    })
    total: number = 0;

    
    @ApiProperty({
        description: 'data',
        required: false,
    })
    data: TransactionHistoryModel[];
}
