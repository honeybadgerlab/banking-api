import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsOptional, Length, Max, Min } from "class-validator";
import { IsUserExist } from "../../user/constraints/is-account-number-unique.constraint";
import { IsAccountNumberUnique } from "../constraints/is-account-number-unique.constraint";
import { IsSupportedCurrency } from "../constraints/is-supported-currency.constraint";

export class CreateAccountModel {
    @ApiProperty({
        description: 'accountNumber, should have the length of 12',
        example: '012345678910',
        required: false,
    })
    @IsOptional()
    @Length(12, 12)
    @IsAccountNumberUnique()
    accountNumber: string;

    @ApiProperty({
        description: 'currency, for now only USD is supported',
        required: true,
        example: 'USD'
    })
    @IsNotEmpty()
    @IsSupportedCurrency()
    currency: string;

    @ApiProperty({
        description: 'initialAmount for account',
        required: true,
        example: '10.0'
    })
    @IsNotEmpty()
    @Min(0)
    @Max(1000000000)
    initialAmount: number;

    @ApiProperty({
        description: 'owner of account',
        required: true,
        example: '1'
    })
    @IsNotEmpty()
    @IsUserExist()
    customerId: number;
}
