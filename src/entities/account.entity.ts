import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { TransactionEntity } from './transaction.entity';
import { UserEntity } from './user.entity';


@Entity({ name: 'account' })
export class AccountEntity {
  @PrimaryGeneratedColumn() id: number;

  @Column({ unique: true }) accountNumber: string;

  @Column({ nullable: false, default: 0, type: 'float' }) balance: number;

  @Column({ nullable: true, default: 'USD' }) currency: string;

  @Column()
  ownerId: number;

  @JoinColumn({ name: 'ownerId' })
  @ManyToOne(
    type => UserEntity,
    user => user.accounts,
    { cascade: true, nullable: false }
  )
  owner: UserEntity;

  @OneToMany(
    type => TransactionEntity,
    transactions => transactions.destination
  )
  inboundTransactions: TransactionEntity[];

  @OneToMany(
    type => TransactionEntity,
    transactions => transactions.source
  )
  outboundTransactions: TransactionEntity[];

  @Column({ nullable: true })
  createdUserId: number;

  @Column({ nullable: true })
  updatedUserId: number;

  @CreateDateColumn({ type: 'timestamp with time zone' }) createdAt: Date;

  @UpdateDateColumn({ type: 'timestamp with time zone' }) updatedAt: Date;
}
