import { Exclude } from 'class-transformer';
import {
  Column,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { RolesEnum } from '../common/constants/roles.enum';
import { AccountEntity } from './account.entity';

@Entity({ name: 'user' })
export class UserEntity {
  @PrimaryGeneratedColumn() id: number;

  @Column() name: string;

  @Column({ unique: true }) username: string;

  @Column({ type:'text', nullable: true, default: RolesEnum.Customer }) role: RolesEnum;

  @Exclude({ toPlainOnly: true }) @Column({ nullable: true }) password: string;

  @Exclude({ toPlainOnly: true }) @Column() salt: string;

  @OneToMany(
    type => AccountEntity,
    accounts => accounts.owner
  )
  accounts: AccountEntity[];

}
