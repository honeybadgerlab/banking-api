import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { TransactionTypesEnum } from '../common/constants/transaction-types.enum';
import { AccountEntity } from './account.entity';


@Entity({ name: 'transaction' })
export class TransactionEntity {
  @PrimaryGeneratedColumn() id: number;

  @Column({ nullable: true }) sourceId: number;

  @JoinColumn({ name: 'sourceId' })
  @ManyToOne(
    type => AccountEntity,
    account => account.outboundTransactions,
    { cascade: true, nullable: true, onDelete: 'CASCADE' }
  )
  source: AccountEntity;

  @Column({ nullable: true }) destinationId: number;

  @JoinColumn({ name: 'destinationId' })
  @ManyToOne(
    type => AccountEntity,
    account => account.outboundTransactions,
    { cascade: true, nullable: true, onDelete: 'CASCADE' }
  )
  destination: AccountEntity;

  @Column() amount: number;

  @Column() currency: string;

  @Column({ nullable: true, type: 'text' }) transactionType: TransactionTypesEnum;

  @Column({ nullable: true, default: '' }) note: string;

  @Column({ nullable: true })
  createdUserId: number;

  @Column({ nullable: true })
  updatedUserId: number;

  @CreateDateColumn({ type: 'timestamp with time zone' }) createdAt: Date;

  @UpdateDateColumn({ type: 'timestamp with time zone' }) updatedAt: Date;
}
