import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserEntity } from '../entities/user.entity';
import { UserService } from './user.service';

describe('UserService', () => {
  let userService: UserService;
  let userRepository: Repository<UserEntity>;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [],
      providers: [
        UserService,
        {
          provide: getRepositoryToken(UserEntity),
          useValue: {
            findOne: jest.fn(),
          },
        },
      ],
    }).compile();

    userService = app.get<UserService>(UserService);
    userRepository = app.get<Repository<UserEntity>>(getRepositoryToken(UserEntity));
  });

  it('should be defined', () => {
    expect(userService).toBeDefined();
  });

  describe('User service find by id', () => {
    it('It should call userRepository.findOne', async () => {
      userService.findById(1);
      expect(userRepository.findOne).toHaveBeenCalledTimes(1);
    });
  });

  describe('User service findOneByUserName', () => {
    it('It should call userRepository.findOneByUserName', async () => {
      userService.findOneByUserName('username');
      expect(userRepository.findOne).toHaveBeenCalledTimes(1);
    });
  });
});
