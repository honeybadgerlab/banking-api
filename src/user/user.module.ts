import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm/dist';

import { UserEntity } from '../entities/user.entity';
import { IsUserExistConstraint } from './constraints/is-account-number-unique.constraint';
import { UserService } from './user.service';

@Module({
  controllers: [],
  exports: [TypeOrmModule, UserService, IsUserExistConstraint],
  imports: [
    TypeOrmModule.forFeature([
      UserEntity,
    ]),
  ],
  providers: [
    UserService,
    IsUserExistConstraint
  ],
})
export class UserModule { }
