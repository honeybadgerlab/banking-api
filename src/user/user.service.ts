import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { UserEntity } from '../entities/user.entity';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserEntity) private userRepository: Repository<UserEntity>,
  ) { }

  async findOneByUserName(username: string) {
    return await this.userRepository.findOne({ username: username });
  }

  async findById(id) {
    return await this.userRepository.findOne(id);
  }
}
