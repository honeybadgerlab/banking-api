import { ApiProperty } from '@nestjs/swagger';

export class UserModel {
    @ApiProperty({
        description: 'name',
        example: 'John Doe'
    })
    name: string;

    @ApiProperty({
        description: 'id',
        example: '1'
    })
    id: number;
}
