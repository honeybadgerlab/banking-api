import { Injectable } from '@nestjs/common';
import {
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';

import { UserService } from '../user.service';

@ValidatorConstraint({ async: true, name: 'IsUserExist' })
@Injectable()
export class IsUserExistConstraint implements ValidatorConstraintInterface {

  constructor(private userService: UserService) { }

  defaultMessage(validationArguments?: ValidationArguments): string {
    return `User is not exist`;
  }

  async validate(userId: number, validationArguments?: ValidationArguments): Promise<boolean> {
    const user = await this.userService.findById(userId);

    return user ? true : false;
  }
}

export function IsUserExist(validationOptions?: ValidationOptions) {
  return function (object: unknown, propertyName: string) {
    registerDecorator({
      constraints: [],
      options: validationOptions,
      propertyName,
      target: object.constructor,
      validator: IsUserExistConstraint,
    });
  };
}
