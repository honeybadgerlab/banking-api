import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AccountModule } from './account/account.module';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { SeederModule } from './seeder/seeder.module';
import { UserModule } from './user/user.module';

function loadEnvFilePath() {
  const env = process.env.NODE_ENV;

  if (!env) {
    return '.env';
  }

  return `.env.${env}`;
}
@Module({
  imports: [
    ConfigModule.forRoot({ envFilePath: loadEnvFilePath(), isGlobal: true }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (
      ) => {
        const databaseConfig = {
          type: 'postgres',
          host: process.env.DATABASE_HOST,
          port: parseInt(process.env.DATABASE_PORT, 10) || 5432,
          username: process.env.DATABASE_USER,
          password: process.env.DATABASE_PASS,
          database: process.env.DATABASE_NAME || 'test',
          entities: [],
          synchronize: true,
        }

        return {
          ...databaseConfig,
          autoLoadEntities: true,
          keepConnectionAlive: true,
          logging: false,
          synchronize: true,
          type: 'postgres',
        };
      },
    }),
    UserModule,
    SeederModule,
    AccountModule,
    AuthModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
