import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { CurrentUserModel } from '../models/current-user.model';

export const User = createParamDecorator((data: unknown, ctx: ExecutionContext): CurrentUserModel => {
  const request = ctx.switchToHttp().getRequest();
  return request.user as CurrentUserModel;
});
