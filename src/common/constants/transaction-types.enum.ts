export enum TransactionTypesEnum {
    'Deposit' = 'Deposit',
    'Withdraw' = 'Withdraw',
    'TransferMoney' = 'TransferMoney',
}