import { ValidationPipe } from '@nestjs/common';
import { ValidationException } from '../exceptions/validation.exception';


export class BaseValidationPipe extends ValidationPipe {
  constructor(groups?: string[]) {
    super({
      exceptionFactory: errors => new ValidationException(errors),
      groups: groups,
      transform: true,
      whitelist: true,
    });
  }
}
