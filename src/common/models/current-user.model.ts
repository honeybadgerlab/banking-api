export interface CurrentUserModel{
    id: number;
    role?: string;
}