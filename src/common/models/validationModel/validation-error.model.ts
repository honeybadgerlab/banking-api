import { ApiBody, ApiProperty } from '@nestjs/swagger';

export class ValidationErrorModel {
  @ApiProperty({
    description: 'errors',
    example: 'Array<any>',
    isArray: true,
  })
  readonly errors: Array<any>;

  @ApiProperty({
    description: 'validationError',
  })
  readonly validationError: boolean;
}
