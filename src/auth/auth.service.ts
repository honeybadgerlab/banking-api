import { Injectable, UnauthorizedException } from '@nestjs/common';


import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import * as crypto from 'crypto';
import { UserService } from '../user/user.service';


@Injectable()
export class AuthService {
  constructor(
    private jwtService: JwtService,
    private configService: ConfigService,
    private userService: UserService
  ) { }

  async validateUser(username: string, password: string): Promise<any> {
    const dbUser = await this.userService.findOneByUserName(username);

    if (!dbUser) {
      throw new UnauthorizedException('Incorrect username or password');
    }
    const currentHash = crypto
      .createHmac('sha256', dbUser.salt)
      .update(password)
      .digest('hex');

    if (dbUser.password !== currentHash) {
      throw new UnauthorizedException('Incorrect username or password');
    }    
    return dbUser;
  }

  async getAccessToken(user: { id: number, role: string }, expiresIn = '24h') {
    const payload = {
      id: user.id,
      role: user.role
    };
    return this.jwtService.sign(payload, {
      expiresIn: expiresIn,
      secret: this.configService.get('JWT_SECRET'),
    });
  }
}
