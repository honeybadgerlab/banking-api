import { Test, TestingModule } from '@nestjs/testing';
import { UserService } from '../user/user.service';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';

describe('AuthController', () => {
  let authController: AuthController;
  let authService: AuthService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
      ],
      controllers: [AuthController],
      providers: [
        {
          provide: UserService,
          useValue: {
            findOneByUserName: jest.fn(),
          },
        },
        {
          provide: AuthService,
          useValue: {
            getAccessToken: jest.fn(() => 'test'),
            validateUser: jest.fn(),
          },
        },
      ],
    }).compile();

    authController = module.get<AuthController>(AuthController);
    authService = module.get<AuthService>(AuthService);
  });

  it('should be defined', () => {
    expect(authController).toBeDefined();
  });

  describe('login', () => {
    it('It should call get access token from auth service', async () => {
      const user = { id: 1, role: 'customer' };

      authController.login(user, { username: 'customer', password: '123456' });
      expect(authService.getAccessToken).toHaveBeenCalled();
    });
  });
});
