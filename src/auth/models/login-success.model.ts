import { ApiProperty } from '@nestjs/swagger';

export class LoginSuccessModel {
  @ApiProperty({
    description: 'accessToken',
  })
  readonly accessToken: string;
}
