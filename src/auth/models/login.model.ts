import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class LoginModel {
  @ApiProperty({
    description: 'username',
    required: true,
  })
  @IsNotEmpty()
  readonly username: string;

  @ApiProperty({
    description: 'password',
    required: true,
  })
  @IsNotEmpty()
  readonly password: string;
}
