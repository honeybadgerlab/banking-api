import { Test, TestingModule } from '@nestjs/testing';
import { UserService } from '../user/user.service';
import { AuthService } from './auth.service';
import * as crypto from 'crypto';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { JwtStrategy } from './jwt.strategy';
import { PassportModule } from '@nestjs/passport';
import { UnauthorizedException } from '@nestjs/common';

describe('AuthService', () => {
  let authService: AuthService;
  let userService: UserService;
  let jwtService: JwtService;
  let jwtStrategy: JwtStrategy;

  beforeEach(async () => {
    let password = '123456';
    const currentHash = crypto
      .createHmac('sha256', '1')
      .update(password)
      .digest('hex');
    const app: TestingModule = await Test.createTestingModule({
      controllers: [],
      imports: [
        ConfigModule,
        JwtModule.register({
          secret: 'test',
          signOptions: { expiresIn: '24h' },
        }),
      ],
      providers: [
        AuthService,
        {
          provide: UserService,
          useValue: {
            findOneByUserName: jest.fn((username) => {
              if (username) {
                return {
                  username,
                  role: 'customer',
                  salt: '1',
                  password: currentHash,
                };
              }
              return null;
            }),
          },
        },
        {
          provide: ConfigService,
          useValue: {
            get: jest.fn((key: string) => {
              // this is being super extra, in the case that you need multiple keys with the `get` method
              if (key === 'JWT_SECRET') {
                return 'test';
              }
              return null;
            }),
          },
        },
        JwtStrategy,
      ],
    }).compile();

    authService = app.get<AuthService>(AuthService);
    userService = app.get<UserService>(UserService);
    jwtService = app.get<JwtService>(JwtService);
    jwtStrategy = app.get<JwtStrategy>(JwtStrategy);
  });

  it('should be defined', () => {
    expect(authService).toBeDefined();
  });

  it('User Service should be defined', () => {
    expect(userService).toBeDefined();
  });

  it('JwtService should be defined', () => {
    expect(jwtService).toBeDefined();
  });

  it('JwtStrategy should be defined', () => {
    expect(jwtStrategy).toBeDefined();
  });

  describe('getAccessToken', () => {
    it('It generates access token', async () => {
      const accessToken = await authService.getAccessToken({
        id: 1,
        role: 'customer',
      });
      const token = jwtService.sign(
        { id: 1, role: 'customer' },
        { expiresIn: '24h', secret: 'test' },
      );
      expect(accessToken).toEqual(token);
    });
  });

  describe('validateUser', () => {
    it('It returns the user in case password is correct', async () => {
      const user = await authService.validateUser('test', '123456');
      expect(user).toBeDefined();
      expect(user.username).toEqual('test');
      expect(user.role).toBeDefined();
    });

    it('It throws the error in case password is not correct', async () => {
      try {
        await authService.validateUser('test', '1234567');
        // Fail test if above expression doesn't throw anything.
        expect(true).toBe(false);
      } catch (e) {
        expect(e.message).toBe('Incorrect username or password');
      }
    });

    it('It throws the error in case username is not correct', async () => {
      try {
        await authService.validateUser('', '123456');
        // Fail test if above expression doesn't throw anything.
        expect(true).toBe(false);
      } catch (e) {
        expect(e.message).toBe('Incorrect username or password');
      }
    });
  });
});
