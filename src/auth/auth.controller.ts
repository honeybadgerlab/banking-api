import {
  Body,
  Controller,
  HttpStatus,
  Post,
  UseGuards,
} from '@nestjs/common';
import { ApiBadRequestResponse, ApiResponse, ApiTags } from '@nestjs/swagger';
import { User } from '../common/decorators/user.decorator';
import { LocalAuthGuard } from '../common/guards/local-auth.guard';
import { ValidationErrorModel } from '../common/models/validationModel/validation-error.model';

import { AuthService } from './auth.service';
import { LoginSuccessModel } from './models/login-success.model';
import { LoginModel } from './models/login.model';

@ApiTags('auth')
@Controller('auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
  ) { }

  @UseGuards(LocalAuthGuard)
  @Post('login')
  @ApiResponse({ description: 'Login Success', status: HttpStatus.OK, type: LoginSuccessModel })
  @ApiBadRequestResponse({ description: 'Validation error', type: ValidationErrorModel })
  async login(@User() user, @Body() loginModel: LoginModel) {
    const accessToken = await this.authService.getAccessToken(user);
    return { accessToken };
  }
}
